from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    todo_list_create,
    todo_list_edit,
    todo_list_delete,
    todo_item_create,
    todo_item_update,
)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todos"),
    path("create/", todo_list_create, name="create_list"),
    path("<int:id>/edit/", todo_list_edit, name="edit_list"),
    path("<int:id>/delete/", todo_list_delete, name="delete_list"),
    path("items/create/", todo_item_create, name="create_item"),
    path("items/<int:id>/edit/", todo_item_update, name="update_item"),
]
