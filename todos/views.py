from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)

    context = {"tasks": tasks}
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_list")

    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    else:
        form = TodoListForm

    context = {"form": form}
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect(todo_list_list)
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            id = todo_item.list.id
            return redirect("todos", id)

    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todos/createItem.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            id = todo_item.list.id
            return redirect("todos", id)

    else:
        form = TodoItemForm

    context = {"form": form}
    return render(request, "todos/editItem.html", context)
